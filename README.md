# Update script
## About
A very simple script to update the software on the Gigabyte Brix. Calls 
```
sudo apt-get update
sudo apt-get upgrade
```
and goes through each project downloaded via git and pulls the latest code. It will also check to ensure that internet connection is present, and that root permissions have been granted
## Improvements
Suggested improvements include allowing the script to update items individually (so that a single failed update does not stop the rest of the script), as well in parallel.
Also, error checking and auto-debugging would improve robustness.
Ultimately, this script should be run in the background every time the user connects to the internet. 
This may be accomplished by placing the script (or a call to the script) in the folder `/etc/NetworkManager/dispatcher.d/`. See '[here](https://askubuntu.com/questions/13963/call-script-after-connecting-to-a-wireless-network)' for more details.