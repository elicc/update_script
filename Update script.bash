#!/bin/bash

## Setup
function check_internet {
	INTERNET=0
	while [ $INTERNET -eq 0 ]; do
	  wget -q --spider http://www.ros.org/
	  if [ $? -eq 0 ]; then
		INTERNET=1
	  else
		echo "Please connect to the internet."
		read -p "Press enter to continue."
	  fi
	done
}

check_internet

ROOT=0
while [ $ROOT -eq 0 ]; do
  sudo apt-get update
  if sudo -n true 2>/dev/null; then
	ROOT=1
  fi
done

echo "y" | sudo apt-get upgrade

check_internet

## g2o
cd ~/g2o
git pull

check_internet

## GTSAM
cd ~/gtsam  
git pull

check_internet

## libfreenect2
cd ~/libfreenect2
git pull

check_internet

## rtabmap
cd ~/rtabmap
git pull

check_internet

## install_isolated
cd ~/catkin_ws/install_isolated
git pull